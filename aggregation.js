//  [SECTION]   MongoDB Aggregation

/* 

    - Used to generate, manipulate and perform operation to create filtered results that helps us to analyze the data.

*/



/* 
    Using the aggregate methid:

        - The "$match" is used to pass the document that meet the specified condition(s) to the next stage/aggregation process.

        Syntax:

            {$math: {field: value}}


        - The "$group"

        Syntax:

            {$group: {_id: "value", fieldResult: "valueResult"}}
*/

// $match
db.fruits.aggregate([
    {
        $match: {onSale: true}
    }
]);



// $group
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", total:{$sum: "$stock"}}}
]);


//  $project

db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", total:{$sum: "$stock"}}},
    {$project: {_id: 0}}
]);

// Sorting Aggregated results

/* 
    - The "$sort" can be used to change the order of the aggregated results.

    Syntax:

        {$sort: {field: 1/-1}}
        1 => lowest to highest
        -1 => highest to lowest

*/

db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", total:{$sum: "$stock"}}},
    {$sort: {total: -1}}
]);


// Aggregating results based on array fields

/* 
    -  The "$unwind" deconstructs an array field from a collection/field with an array value to output a result.

        Syntax:

            {$unwind: field}

*/

db.fruits.aggregate([
    {$unwind: "$origin"}
]);


db.fruits.aggregate([
    {$unwind: "$origin"},
    {$group: {_id: "$origin", kinds:{$sum: 1}}}
]);



//  [SECTION]   Other Aggragate stage

//  $count all yellow fruits
db.fruits.aggregate([
    {$match: {color: "Yellow"}},
    {$count: "Yellow Fruits"}
]);


// $avg gets the average value of stock

db.fruits.aggregate([
    {$match: {color: "Yellow"}},
    {$group: {_id: "$color", yellow_fruits_stock:{$avg: "$stock"}}}
]);


// $min & $max
db.fruits.aggregate([
    {$match: {color: "Yellow"}},
    {$group: {_id: "$color", yellow_fruits_stock:{$max: "$stock"}}}
]);